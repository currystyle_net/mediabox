//
//  Mediator.m
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "Mediator.h"
#import "RootViewController.h"
#import "Page.h"
#import "Visual.h"
#import "SettingViewController.h"
#import "ChkApplicationOptional.h"
#import "VknPostCommand.h"
#import "SplashViewController.h"
#import "Pager.h"
#import "NADView.h"

#define ADSTIR_MEDIA_ID @"MEDIA-2f226b5f"
#define ADSTIR_BANNER_SPOT_ID 1

#define NEND_API_KEY @"c4c47e2e869f17cdb4c6582b61b6c7eca3d8666b"
#define NEND_SPOT_ID @"212164"

@interface Mediator ()<
PageDelegate
, UIScrollViewDelegate
, SettingViewDelegate
, UIAlertViewDelegate
>{
    UIWindow* window;
    RootViewController* rootViewController;
    UIScrollView* mainSv;
    UIView* hideView;
    NSArray* pageList;
    UIView* adViewWrapper;
    UIButton* arrowUp;
    UIButton* arrowDown;
    UIButton* reloadButton;
    UIButton* settingButton;
    SiteList* siteList;
    int currentpage;
//    AdstirMraidView* adView;
    NADView* adView;
    BOOL loading;
    Pager* pager;
}

@end


@implementation Mediator

-(id) initWithWindow:(UIWindow*)window_{
    self = [super init];
    if(self){
        window = window_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    [BiralUtil tryShowRecommend];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [UIApplication sharedApplication].statusBarHidden = NO;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    rootViewController = [[RootViewController alloc] init];
    window.rootViewController = rootViewController;
    
    UIView* rootView = rootViewController.view;
    rootView.backgroundColor = [UIColor blackColor];
    
    CGRect pageFrame = CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]-50);
    hideView = [[UIView alloc] initWithFrame:pageFrame];
    [rootView addSubview:hideView];
    
    mainSv = [[UIScrollView alloc] initWithFrame:pageFrame];
    mainSv.delegate = self;
    mainSv.pagingEnabled = YES;
    mainSv.showsVerticalScrollIndicator = NO;
    [rootView addSubview:mainSv];
    
    pager = [[Pager alloc] init];
    [rootView addSubview:pager];
    
    [BiralUtil setup:rootViewController mainSv:mainSv pageDelegate:self];
    
    [rootViewController showLoading];
    [self loadModel];
    [BiralUtil trackScreen:@"top"];
    
    SplashViewController* splashViewController = [[SplashViewController alloc] initWithDelegate:self];
    [rootView addSubview:splashViewController.view];
}

-(void) loadModel{
    if(loading) return;
    __weak Mediator* SELF = self;
    [[Model getInstance] load:^(SiteList *siteList_) {
        siteList = siteList_;
        [SELF createViews];
        [SELF reloadPages];
        [pager reload:siteList];
        [rootViewController hideLoading];
        loading = NO;
    } failCallback:^{
        UIAlertView* alertView = [[UIAlertView alloc] init];
        alertView.title = @"通信に失敗しました";
        alertView.message = @"時間を置いてから再度お試しください";
        [alertView addButtonWithTitle:@"リトライ"];
        alertView.delegate = self;
        alertView.tag = 1;
        [alertView show];
        loading = NO;
    }];
    
     [Ad requestData];
    
    loading = YES;
}

-(void) reloadModel{
    if(loading) return;
    __weak Mediator* SELF = self;
    [rootViewController showLoading];
    [[Model getInstance] load:^(SiteList *siteList_) {
        siteList = siteList_;
        [SELF reloadPages];
        [pager reload:siteList];
        [rootViewController hideLoading];
        loading = NO;
        NSLog(@"reloaded");
    } failCallback:^{
        [SELF reloadPages];
        [rootViewController hideLoading];
        loading = NO;
        NSLog(@"reloaded fail");
    }];
    
    [Ad requestData];
    loading = YES;
}

-(CGRect)pageFrame{
    return CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]-50);
}

-(void) createViews{
    CGRect pageFrame = [self pageFrame];
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    for(int i=0,max=[siteList count];i<max;i++){
        Page* page = [[Page alloc] initWithDelegate:self];
        [tmp addObject:page];
        UIView* pageView = [page getView];
        pageView.frame = pageFrame;
        [mainSv addSubview:pageView];
        pageFrame.origin.y += pageFrame.size.height;
    }
    pageList = [NSArray arrayWithArray:tmp];
    
    mainSv.contentSize = CGSizeMake(320, pageFrame.origin.y);
    
    adViewWrapper = [[UIView alloc] initWithFrame:CGRectMake(0, [VknDeviceUtil windowHeight]-50.0f, 320, 50.0f)];
    adViewWrapper.backgroundColor = UIColorFromHex(0x000000);
    adViewWrapper.hidden = YES;
    adView = [[NADView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    
    // test
//    [adView setNendID:@"a6eca9dd074372c898dd1df549301f277c53f2b9" spotID:@"3172"];
    [adView setNendID:NEND_API_KEY spotID:NEND_SPOT_ID];
    [adViewWrapper addSubview:adView];
    [adView load];
    
    [rootViewController.view addSubview:adViewWrapper];
}

-(void) reloadPages{
    int wait = 0;
    for(int i=0,max=[pageList count];i<max;i++){
        Page* page = [pageList objectAtIndex:i];
        [VknUtils wait:wait callback:^{
            [self reloadPage:i];
        }];
        wait += 0.2f;
    }
    
    [VknUtils wait:wait callback:^{
//        adView = [[AdstirMraidView alloc] initWithAdSize:kAdstirAdSize320x50 media:ADSTIR_MEDIA_ID spot:ADSTIR_BANNER_SPOT_ID];
//        adView.intervalTime = 10;
//        [adViewWrapper addSubview:adView];
        
        [Visual startTimer];
    }];
}

-(void) reloadPage:(int)index{
    Site* site = [siteList get:index];
    Page* page = [pageList objectAtIndex:index];
    [page reload:site];
}

-(void) setOverlay{
    CGRect buttonFrame = CGRectMake(320-50, 20, 50, 46);
    
    float margin = 46;
    
    reloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [reloadButton setImage:[UIImage imageNamed:@"reloadButton"] forState:UIControlStateNormal];
    [reloadButton addTarget:self action:@selector(tapReload:) forControlEvents:UIControlEventTouchUpInside];
    reloadButton.frame = buttonFrame;
    buttonFrame.origin.y += margin;
    [rootViewController.view addSubview:reloadButton];
    
//    UIButton* rankButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [rankButton setImage:[UIImage imageNamed:@"rankButton"] forState:UIControlStateNormal];
//    [rankButton addTarget:self action:@selector(tapRank:) forControlEvents:UIControlEventTouchUpInside];
//    rankButton.frame = buttonFrame;
//    buttonFrame.origin.y += margin;
//    [rootViewController.view addSubview:rankButton];
    
    settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [settingButton setImage:[UIImage imageNamed:@"settingButton"] forState:UIControlStateNormal];
    [settingButton addTarget:self action:@selector(tapSetting:) forControlEvents:UIControlEventTouchUpInside];
    settingButton.frame = buttonFrame;
    buttonFrame.origin.y += margin;
    [rootViewController.view addSubview:settingButton];
    
    arrowUp = [UIButton buttonWithType:UIButtonTypeCustom];
    [arrowUp setImage:[UIImage imageNamed:@"arrowUp"] forState:UIControlStateNormal];
    [arrowUp addTarget:self action:@selector(tapUp) forControlEvents:UIControlEventTouchUpInside];
    arrowUp.frame = CGRectMake(141.5f, 20, 36.5f, 26.5f);
    arrowUp.hidden = YES;
    [rootViewController.view addSubview:arrowUp];
    
    arrowDown = [UIButton buttonWithType:UIButtonTypeCustom];
    [arrowDown setImage:[UIImage imageNamed:@"arrowDown"] forState:UIControlStateNormal];
    [arrowDown addTarget:self action:@selector(tapDown) forControlEvents:UIControlEventTouchUpInside];
    arrowDown.frame = CGRectMake(141.5f, [VknDeviceUtil windowHeight]-76.5f, 36.5f, 26.5f);
    [rootViewController.view addSubview:arrowDown];
}

-(void) tapUp{
    mainSv.scrollEnabled = NO;
    [mainSv setContentOffset:CGPointMake(0, mainSv.contentOffset.y-mainSv.frame.size.height) animated:YES];
    [VknUtils wait:0.5f callback:^{
        [self scrollViewDidEndDecelerating:mainSv];
    }];
}

-(void) tapDown{
    mainSv.scrollEnabled = NO;
    [mainSv setContentOffset:CGPointMake(0, mainSv.contentOffset.y+mainSv.frame.size.height) animated:YES];
    [VknUtils wait:0.5f callback:^{
        [self scrollViewDidEndDecelerating:mainSv];
    }];
}

-(void) tapReload:(UIButton*)button{
    [self reloadModel];
}

-(void) tapRank:(UIButton*)button{
    
}

-(void) tapSetting:(UIButton*)button{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    SettingViewController* settingViewController = [[SettingViewController alloc] initWithDelegate:self];
    [rootViewController presentViewController:settingViewController animated:YES completion:^{
        
    }];
}

-(void) willEnterForeground{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [ChkApplicationOptional applicationWillEnterForeground];
    
    [BiralUtil tryShowRecommend];
    [self reloadModel];
}

-(void) didEnterBackground:(UIApplication*)application{
    [ChkApplicationOptional applicationDidEnterBackground:application];
}

-(void) memoryWarning{
    NSLog(@"************memoryWorning************");
    [VknImageCache clear];
}

-(void) didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString* deviceTokenStr = [VknDataUtil deviceTokenToStr:deviceToken];
    NSLog(@"deviceTokenStr:%@", deviceTokenStr);
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSDictionary* param = @{
                            @"devicetoken": deviceTokenStr
                            , @"version": version
                            };
    
    VknPostCommand* c = [[VknPostCommand alloc] initWithValues:param withUrl:[Api url:@"register.php"]];
    c.onComplete = ^(NSData* data){
        NSLog(@"register complete");
    };
    c.onFail = ^(NSError* error){
        NSLog(@"register fail:%@", error.debugDescription);
    };
    [c execute:nil];
}

-(void) showOverlay{
    [UIView animateWithDuration:0.2f animations:^{
        arrowUp.alpha = 1.0f;
        arrowDown.alpha = 1.0f;
        reloadButton.alpha = 1.0f;
        settingButton.alpha = 1.0f;
        pager.alpha = 1.0f;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) hideOverlay{
    [UIView animateWithDuration:0.2f animations:^{
        arrowUp.alpha = 0.0f;
        arrowDown.alpha = 0.0f;
        reloadButton.alpha = 0.0f;
        settingButton.alpha = 0.0f;
        pager.alpha = 0.0f;
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark -------- SplashViewDelegate -----------

-(void) onEndSplash{
    [self setOverlay];
    adViewWrapper.hidden = NO;
}

#pragma mark -------- UIAlertViewDelegate ------------

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 1){ // 初期か失敗
        [self loadModel];
    }else if(alertView.tag == 2){ // レビュー訴求
        
    }
}

#pragma mark -------- UIScrollViewDelegate ----------- 

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    scrollView.scrollEnabled = YES;
    
    float y = scrollView.contentOffset.y;
    int p = y / scrollView.frame.size.height;
    
    arrowUp.hidden = (p == 0);
    arrowDown.hidden = (p == [siteList count]-1);
    
    Page* page = [pageList objectAtIndex:p];
    UIView* pageView = [page getView];
    [page dark:0.0f];
    if(pageView.superview == mainSv) return;
    
    CGRect pageFrame = [self pageFrame];
    pageFrame.origin.y  = pageFrame.size.height * p;
    pageView.frame = pageFrame;
    [mainSv addSubview:pageView];
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView{
    float y = scrollView.contentOffset.y;
    float svHeight = scrollView.frame.size.height;
    int p = y / svHeight;
    int amari = (int)y % (int)svHeight;
    
    float per = (float) amari / svHeight;
    float scale = 1.0f- (per / 6.0f);
    
    CGRect pageFrame = [self pageFrame];
    Page* page = [pageList objectAtIndex:p];
    UIView* pageView = [page getView];
    
    if(pageView.superview != hideView){
        [hideView addSubview:pageView];
        pageView.frame = pageFrame;
    }
    pageView.transform = CGAffineTransformMakeScale(scale, scale);
    [page dark:1.0f-scale];
    
    p = (y+svHeight/2.0f) / svHeight;
    if(p<0) p = 0;
    if(p >= [siteList count]) p = [siteList count]-1;
    [pager page:p];
}

-(void) scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    scrollView.scrollEnabled = NO;
}

@end
