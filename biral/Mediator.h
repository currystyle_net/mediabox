//
//  Mediator.h
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mediator : NSObject

-(id) initWithWindow:(UIWindow*)window_;
-(void) willEnterForeground;
-(void) didEnterBackground:(UIApplication*)application;
-(void) memoryWarning;

-(void) didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;

@end
