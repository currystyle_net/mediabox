//
//  Model.h
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ArticleList.h"
#import "SiteList.h"

typedef void (^ModelLoadCallback_t)(SiteList* siteList);
typedef void (^ModelCallback_t)();

@interface Model : NSObject

+(Model*)getInstance;
-(void) load:(ModelLoadCallback_t)callback failCallback:(ModelCallback_t)failCallback;
-(void) loadLocal:(ModelLoadCallback_t)callback;
-(BOOL) isInitailized;

@end
