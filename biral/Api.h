//
//  Api.h
//  mediabox
//
//  Created by kawase yu on 2014/07/27.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Api : NSObject

+(NSString*)url:(NSString*)path;

@end
