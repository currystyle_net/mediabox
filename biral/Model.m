//
//  Model.m
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "Model.h"
#import "VknUrlLoadCommand.h"

@interface Model (){
    ArticleList* articleList;
    SiteList* siteList;
}

@end

@implementation Model

+ (Model*)getInstance {
    static Model* sharedSingleton;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[Model alloc]
                           initSharedInstance];
    });
    return sharedSingleton;
}

- (id)initSharedInstance {
    self = [super init];
    if (self) {

    }
    return self;
}

- (id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

static NSString* const cacheApiJsonKey = @"cacheApiJsonKey";
-(void) load:(ModelLoadCallback_t)callback failCallback:(ModelCallback_t)failCallback{
    int r = [VknUtils getEpochSeconds];
    VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:[Api url:[NSString stringWithFormat:@"api.json?%d", r]]];
    NSLog(@"%@", [Api url:@"api.json"]);
    c.onComplete = ^(NSData* data){
//        NSLog(@"%@", [VknDataUtil dataToString:data]);
        NSString* jsonStr = [VknDataUtil dataToString:data];
        NSDictionary* json = [VknDataUtil dataToJson:data];
        if(json == nil) return;
        siteList = [[SiteList alloc] initWithArray:[json objectForKey:@"site"]];
        articleList = [[ArticleList alloc] initWithArray:[json objectForKey:@"article"]];
        [self mergeSite:articleList];
        [self mergeArticle:siteList];
        
        [VknThreadUtil mainBlock:^{
            callback(siteList);
        }];
        
        NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:jsonStr forKey:cacheApiJsonKey];
        [ud setBool:YES forKey:modelInitializedKey];
        [ud synchronize];
    };
    
    c.onFail = ^(NSError* error){
        [VknThreadUtil mainBlock:^{
            if([self isInitailized]){
                [self loadLocal:callback];
            }else{
                failCallback();
            }
        }];
    };
    
    [c execute:nil];
}

-(void) loadLocal:(ModelLoadCallback_t)callback{
    NSLog(@"loadLocal");
    NSString* jsonString = [[NSUserDefaults standardUserDefaults] stringForKey:cacheApiJsonKey];
    NSDictionary* json = [VknDataUtil strToJson:jsonString];
    siteList = [[SiteList alloc] initWithArray:[json objectForKey:@"site"]];
    articleList = [[ArticleList alloc] initWithArray:[json objectForKey:@"article"]];
    [self mergeSite:articleList];
    [self mergeArticle:siteList];
    callback(siteList);
}

-(void) mergeSite:(ArticleList*)articleList_{
    for(int i=0,max=[articleList_ count];i<max;i++){
        Article* article = [articleList_ get:i];
        [self mergeSiteRow:article];
    }
}

-(void) mergeSiteRow:(Article*)article{
    for(int i=0,max=[siteList count];i<max;i++){
        Site* site = [siteList get:i];
        if(article.siteId == site.id_){
            article.site = site;
            return;
        }
    }
}

-(void) mergeArticle:(SiteList*)siteList{
    for(int i=0,max=[siteList count];i<max;i++){
        Site* site = [siteList get:i];
        int siteId = site.id_;
        ArticleList* articleList_ = [[ArticleList alloc] init];
        for(int j=0,jMax=[articleList count];j<jMax;j++){
            Article* article = [articleList get:j];
            if(article.siteId == siteId){
                [articleList_ add:article];
            }
        }
        
        site.articleList = articleList_;
    }
}

static NSString* const modelInitializedKey = @"modelInitializedKey";
-(BOOL) isInitailized{
    return [[NSUserDefaults standardUserDefaults] boolForKey:modelInitializedKey];
}

@end
