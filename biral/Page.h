//
//  Page.h
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PageDelegate.h"

@interface Page : NSObject

-(id) initWithDelegate:(NSObject<PageDelegate>*)delegate_;
-(UIView*) getView;
-(void) reload:(Site*)site;
-(void) dark:(float)rate;

@end
