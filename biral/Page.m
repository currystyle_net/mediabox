//
//  Page.m
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "Page.h"
#import "ArticleListViewController.h"
#import "ArticleDetailViewController.h"
#import "AdArticleViewController.h"

@interface Page ()<
ArticleListViewDelegate
, ArticleDetailViewDelegate
, AdArticleViewDelegate>{
    __weak NSObject<PageDelegate>* delegate;
    UIView* view;
    UINavigationController* navigationController;
    ArticleListViewController* listViewController;
    UIView* blackLayer;
}

@end

@implementation Page

-(id) initWithDelegate:(NSObject<PageDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight])];
    listViewController = [[ArticleListViewController alloc] initWithDelegate:self];
    
    // navigation
    navigationController = [[UINavigationController alloc] initWithRootViewController:listViewController];
    [navigationController setNavigationBarHidden:YES];
    UISwipeGestureRecognizer* reco = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
    reco.direction = UISwipeGestureRecognizerDirectionRight;
//    [navigationController.view addGestureRecognizer:reco];
    
    [view addSubview:navigationController.view];
    
    blackLayer = [[UIView alloc] initWithFrame:view.frame];
    blackLayer.backgroundColor = [UIColor blackColor];
    blackLayer.alpha = 0;
    blackLayer.userInteractionEnabled = NO;
    [view addSubview:blackLayer];
}

-(void) swipeRight{
    NSLog(@"swipe");
    if([navigationController.viewControllers count] < 2) return;
    ArticleDetailViewController* detailViewController = (ArticleDetailViewController*)[navigationController popViewControllerAnimated:YES];
    
    [detailViewController die];
}

-(void) onSelectArticle:(Article *)article{
    [BiralUtil mainSv].scrollEnabled = NO;
    [delegate hideOverlay];
    if([article isAd]){
        AdArticleViewController* detailViewController = [[AdArticleViewController alloc] initWithDelegate:self];
        [navigationController pushViewController:detailViewController animated:YES];
        [VknThreadUtil mainBlock:^{
            [detailViewController load:article];
        }];
    }else{
        [BiralUtil read:article.id_];
        article.readed = YES;
        ArticleDetailViewController* detailViewController = [[ArticleDetailViewController alloc] initWithDelegate:self];
        [navigationController pushViewController:detailViewController animated:YES];
        [VknThreadUtil mainBlock:^{
            [detailViewController load:article];
        }];
    }
}


-(UIView*) getView{
    return view;
}

-(void) reload:(Site*)site{
    [listViewController reload:site];
}

-(void) dark:(float)rate{
    blackLayer.alpha = rate * 5.0f;
}

@end
