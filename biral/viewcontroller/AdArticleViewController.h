//
//  AdArticleViewController.h
//  biral
//
//  Created by kawase yu on 2014/07/26.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "BaseViewController.h"
#import "AdArticle.h"

@protocol AdArticleViewDelegate <NSObject>

@end

@interface AdArticleViewController : BaseViewController

-(id) initWithDelegate:(NSObject<AdArticleViewDelegate>*)delegate_;
-(void) load:(AdArticle*)article;

@end
