//
//  SettingViewController.m
//  mediabox
//
//  Created by kawase yu on 2014/07/27.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "SettingViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface SettingViewController ()<
UITableViewDataSource
, UITableViewDelegate
, UIPickerViewDataSource
, UIPickerViewDelegate
>{
   __weak  NSObject<SettingViewDelegate>* delegate;
    UITableView* tableView_;
    NSArray* twAccounts;
    NSArray* fbAccounts;
    int pickerType;
    UIView* overlay;
    UIPickerView* pickerView;
}

@end

static const int pickerTypeTw = 1;
static const int pickerTypeFb = 2;

@implementation SettingViewController

-(id) initWithDelegate:(NSObject<SettingViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    [self setTableView];
    [self setHeaderView];
    [self setOverlay];
}

-(void) setTableView{
    tableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]) style:UITableViewStyleGrouped];
    tableView_.delegate = self;
    tableView_.dataSource = self;
    tableView_.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    [self.view addSubview:tableView_];
}

-(void) setHeaderView{
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    UIToolbar* toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    [headerView addSubview:toolBar];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 320, 44)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont systemFontOfSize:14.0f];
    titleLabel.text = @"設定";
    [headerView addSubview:titleLabel];
    
    UIButton* xButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [xButton setImage:[UIImage imageNamed:@"xButton"] forState:UIControlStateNormal];
    [xButton addTarget:self action:@selector(tapX) forControlEvents:UIControlEventTouchUpInside];
    xButton.frame = CGRectMake(320-42, 22, 40, 40);
    [headerView addSubview:xButton];
    
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, 63.5f, 320, 0.5f)];
    line.backgroundColor = UIColorFromHex(0x777777);
    [headerView addSubview:line];
    
    [self.view addSubview:headerView];
}

-(void) setOverlay{
    overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight])];
    overlay.backgroundColor = [UIColor blackColor];
    overlay.alpha = 0;
    overlay.userInteractionEnabled = NO;
    [overlay addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePicker)]];
    [self.view addSubview:overlay];
}

-(void) tapX{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark -------- TableViewDelegate -----------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
    
//    switch (section) {
//        case 0: return 2;
//        case 1: return 5;
//    }
//    
//    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellIdentifier = @"articleCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = [self titleForIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    if(indexPath.row == 4){ // バージョンのときはなし
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.detailTextLabel.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
    
//    if(indexPath.section == 0){
//        if(indexPath.row == 0){ // tw
//            NSLog(@"requestTwAccount");
//            [BiralUtil currntTwAccount:^(ACAccount *account) {
//                NSLog(@"callback:%@", account);
//                cell.detailTextLabel.text = account.username;
//            }];
//        }else if(indexPath.row == 1){ // fb
//            
//        }
//    }
//    
//    return cell;
}


-(NSString*)titleForIndexPath:(NSIndexPath*)indexPath{
    int section = indexPath.section;
    int row = indexPath.row;
//    if(section == 0){ /* 外部サービス連携 */
//        if(row == 0){ return @"Twitter"; }
//        else if(row==1){ return @"Facebook"; }
//    }else if(section == 1){ /* このアプリについて */
        if(row == 0){ return @"不具合報告・ご意見"; }
        else if(row == 1){ return @"お知らせ"; }
        else if(row == 2){ return @"レビューを書く"; }
//        else if(row == 3){ return @"知人に勧める"; }
        else if(row == 3){ return @"バージョン"; }
//    }
    
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
//    return 2;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"このアプリについて";
//    if(section == 0){
//        return @"外部サービス連携";
//    }else if(section==1){
//        return @"このアプリについて";
//    }
//    
//    return @"";
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    int section = indexPath.section;
    int row = indexPath.row;
    
//    if(section == 0){ // シェア系
//        if(row == 0){
//            [self selectTwAccounts];
//        }else if(row == 1){
//            [self selectFbAccounts];
//        }
//        return;
//    }
    
    // このアプリについて
    if(row == 0){ // 問い合わせ
        [VknUtils openBrowser:@"http://www.currystyle.net/contact"];
    }else if(row == 1){ // お知らせ
        [VknUtils openBrowser:@"http://www.currystyle.net/"];
    }else if(row == 2){ // レビュー
        [VknUtils openBrowser:@"https://itunes.apple.com/us/app/media-box/id903695563?l=ja&ls=1&mt=8"];
    }
//    else if(row == 3){ // 知人に教える
//        NSString *text  = @"MediaBox";
//        NSURL    *url   = [NSURL URLWithString:@"http://www.currystyle.net"];
//        NSArray *activityItems = @[text, url];
//        UIActivityViewController *activityView = [[UIActivityViewController alloc] initWithActivityItems:activityItems
//                                                                                   applicationActivities:@[]];
//        [self presentViewController:activityView animated:YES completion:nil];
//    }
    else if(row==3){ // バージョン
        return;
    }
}

-(void) selectTwAccounts{
    pickerType = pickerTypeTw;
    ACAccountStore *store = [[ACAccountStore alloc] init];
    ACAccountType *type = [store accountTypeWithAccountTypeIdentifier:
                           ACAccountTypeIdentifierTwitter];
    [self showLoading];
    [store requestAccessToAccountsWithType:type
                                   options:nil
                                completion:^(BOOL granted, NSError *error) {
                                    [VknThreadUtil mainBlock:^{
                                        [self hideLoading];
                                    }];
                                    
                                    if (!granted) {
                                        NSLog(@"not granted");
                                        [self noTwAccounts];
                                        return;
                                    }
                                    
                                    NSArray *twitterAccounts = [store accountsWithAccountType:type];
                                    
                                    if (!(twitterAccounts > 0)) {
                                        NSLog(@"no twitter accounts");
                                        return;
                                    }
                                    
                                    twAccounts = twitterAccounts;
                                    [VknThreadUtil mainBlock:^{
                                         [self showPicker];
                                    }];
                                }];
}

-(void) noTwAccounts{
    [VknThreadUtil mainBlock:^{
        UIAlertView* alertView = [[UIAlertView alloc] init];
        alertView.title = @"アカウントが取得できませんでした";
        alertView.message = @"端末の「設定」→「プライバシー」よりTwitterアカウントの利用を許可してください";
        [alertView addButtonWithTitle:@"OK"];
        [alertView show];
    }];
}

-(void) selectFbAccounts{
    pickerType = pickerTypeFb;
    
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.title = @"TODO";
    [alertView addButtonWithTitle:@"OK"];
    [alertView show];
//    ACAccountStore *store = [[ACAccountStore alloc] init];
//    ACAccountType *type = [store accountTypeWithAccountTypeIdentifier:
//                           ACAccountTypeIdentifierFacebook];
//    NSDictionary *options = @{ ACFacebookAppIdKey : _STFacebookAppID,
//                               ACFacebookAudienceKey : ACFacebookAudienceOnlyMe,
//                               ACFacebookPermissionsKey : @[@"email"] };
//    
//    [self showLoading];
//    [store requestAccessToAccountsWithType:type
//                                   options:nil
//                                completion:^(BOOL granted, NSError *error) {
//                                    [VknThreadUtil mainBlock:^{
//                                        [self hideLoading];
//                                    }];
//                                    
//                                    if (!granted) {
//                                        NSLog(@"not granted");
//                                        return;
//                                    }
//                                    
//                                    NSArray *facebookAccounts = [store accountsWithAccountType:type];
//                                    
//                                    if (!(facebookAccounts > 0)) {
//                                        NSLog(@"no twitter accounts");
//                                        return;
//                                    }
//                                    
//                                    fbAccounts = facebookAccounts;
//                                    [VknThreadUtil mainBlock:^{
//                                        [self showPicker];
//                                    }];
//                                }];
}

-(void) showPicker{
    
    NSLog(@"isMainThread:%d", [NSThread isMainThread]);
    
    pickerView = [[UIPickerView alloc] init];
    pickerView.delegate = self;
    pickerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:pickerView];
    
    CGRect frame = pickerView.frame;
    frame.origin.y = [VknDeviceUtil windowHeight];
    pickerView.frame = frame;
    
    [UIView animateWithDuration:0.3f animations:^{
        pickerView.transform = CGAffineTransformMakeTranslation(0, -pickerView.frame.size.height);
        overlay.alpha = 0.8f;
    } completion:^(BOOL finished) {
        overlay.userInteractionEnabled = YES;
    }];
}

-(void) hidePicker{
    [tableView_ reloadData];
    overlay.userInteractionEnabled = NO;
    [UIView animateWithDuration:0.2f animations:^{
        pickerView.transform = CGAffineTransformMakeTranslation(0, 0);
        overlay.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [pickerView removeFromSuperview];
    }];
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return (pickerType == pickerTypeFb) ? [fbAccounts count] : [twAccounts count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerType == pickerTypeFb){
        ACAccount* account = [twAccounts objectAtIndex:row];
        return account.username;
    }else{
        ACAccount* account = [twAccounts objectAtIndex:row];
        return account.username;
    }
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerType == pickerTypeFb){
        ACAccount* account = [fbAccounts objectAtIndex:row];
        [BiralUtil setCurrentFbusername:account.username];
    }else{
        ACAccount* account = [twAccounts objectAtIndex:row];
        [BiralUtil setCurrentTwusername:account.username];
    }
}

@end
