//
//  SplashViewController.h
//  mediabox
//
//  Created by kawase yu on 2014/08/02.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "BaseViewController.h"

@protocol SplashViewDelegate <NSObject>

-(void) onEndSplash;

@end

@interface SplashViewController : BaseViewController

-(id) initWithDelegate:(NSObject<SplashViewDelegate>*)delegate_;

@end
