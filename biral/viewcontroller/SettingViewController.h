//
//  SettingViewController.h
//  mediabox
//
//  Created by kawase yu on 2014/07/27.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "BaseViewController.h"

@protocol SettingViewDelegate <NSObject>

@end

@interface SettingViewController : BaseViewController

-(id) initWithDelegate:(NSObject<SettingViewDelegate>*)delegate_;

@end
