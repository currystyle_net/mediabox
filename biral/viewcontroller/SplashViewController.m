//
//  SplashViewController.m
//  mediabox
//
//  Created by kawase yu on 2014/08/02.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "SplashViewController.h"

@interface SplashViewController (){
    __weak NSObject<SplashViewDelegate>* delegate;
    
    UIImageView* image1;
    UIImageView* image2;
    UIImageView* image3;
    UIImageView* image4;
    UIView* imageLayer;
    UIImageView* ttl;
    UIImageView* copyright;
    int step;
    
    BOOL isEnded;
    int currnt;
}

@end

@implementation SplashViewController


-(id) initWithDelegate:(NSObject<SplashViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    UIView* view = self.view;
    view.backgroundColor = [UIColor blackColor];
    imageLayer = [[UIView alloc] initWithFrame:view.frame];
    image1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 640, 568)];
    image2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 640, 568)];
//    image3 = [[UIImageView alloc] initWithFrame:CGRectMake(-160, 0, 640, 568)];
//    image4 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 640, 568)];
//    [imageLayer addSubview:image4];
//    [imageLayer addSubview:image3];
    [imageLayer addSubview:image2];
    [imageLayer addSubview:image1];
    
    image1.image = [self image];
    image2.image = [self image];
//    image3.image = [self image];
//    image4.image = [self image];
    
    // initial
    image1.alpha = image2.alpha = image3.alpha = image4.alpha = 0.0f;
//    image1.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
//    image2.transform = CGAffineTransformMakeTranslation(-320, 0);
//    image3.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
    
    [view addSubview:imageLayer];
    
    ttl = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splashTtl"]];
    ttl.frame = CGRectMake(57, 44.5f, 193.5f, 77.5f);
    [view addSubview:ttl];
    
    copyright = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splashCopyright"]];
    copyright.frame = CGRectMake(86, [VknDeviceUtil windowHeight]-53.5f, 147.5f, 29);
    [view addSubview:copyright];
    
    [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)]];
    
    image1.transform = CGAffineTransformMakeTranslation(-320, 0);
    [UIView animateWithDuration:2.0f animations:^{
        image1.alpha = 1.0f;
    } completion:^(BOOL finished) {
        image2.alpha = 1.0f;
        [self start];
    }];
}

-(void) tap{
    isEnded = YES;
    
    [UIView animateWithDuration:0.2f animations:^{
        imageLayer.alpha = 0.0f;
        ttl.frame = CGRectMake(57, -77.5f, 193.5f, 77.5f);
        copyright.frame = CGRectMake(84, [VknDeviceUtil windowHeight], 147.5f, 29);
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [delegate onEndSplash];
    }];
}

-(void) start{
    if(isEnded) return;
    [UIView animateWithDuration:7.0f animations:^{
        image1.transform = CGAffineTransformMakeTranslation(0, 0);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:2.0f animations:^{
            image1.alpha = 0.0f;
        } completion:^(BOOL finished) {
            image1.image = [self image];
            image1.alpha = 1.0f;
            image1.transform = CGAffineTransformMakeTranslation(-320, 0);
            [imageLayer addSubview:image2];
            [UIView animateWithDuration:7.0f animations:^{
                image2.transform = CGAffineTransformMakeTranslation(-320, 0);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:2.0f animations:^{
                    image2.alpha = 0;
                } completion:^(BOOL finished) {
                    image2.transform = CGAffineTransformMakeTranslation(0, 0);
                    image2.image = [self image];
                    [imageLayer addSubview:image1];
                    image2.alpha = 1.0f;
                    [self start];
                }];
            }];
        }];
    }];
//    [UIView animateWithDuration:2.0f animations:^{
//        image1.alpha = 1.0f;
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:7.0f animations:^{
//            image1.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
//        } completion:^(BOOL finished) {
//        image2.alpha = 1.0f;
//        [UIView animateWithDuration:2.0f animations:^{
//            image1.alpha = 0.0f;
//        } completion:^(BOOL finished) {
//            [UIView animateWithDuration:7.0f animations:^{
//                image2.transform = CGAffineTransformMakeTranslation(0, 0);
//            } completion:^(BOOL finished) {
//                image3.alpha = 1.0f;
//                [UIView animateWithDuration:2.0f animations:^{
//                    image2.alpha = 0.0f;
//                } completion:^(BOOL finished) {
//                    [UIView animateWithDuration:7.0f animations:^{
//                        image3.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
//                    } completion:^(BOOL finished) {
//                        image4.alpha = 1.0f;
//                        image4.transform = CGAffineTransformMakeTranslation(0, 0);
//                        image4.image = [self image];
//                        [UIView animateWithDuration:2.0f animations:^{
//                            image3.alpha = 0.0f;
//                        } completion:^(BOOL finished) {
//                            [UIView animateWithDuration:7.0f animations:^{
//                                image4.transform = CGAffineTransformMakeTranslation(-320, 0);
//                            } completion:^(BOOL finished) {
//                                [self next];
//                            }];
//                        }];
//                    }];
//                }];
//            }];
//            }];
//        }];
//    }];
}

//-(void) next{
//    image1.alpha = image2.alpha = image3.alpha = 0.0f;
//    image1.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
//    image2.transform = CGAffineTransformMakeTranslation(-320, 0);
//    image3.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
//    
//    image1.image = [self image];
//    image2.image = [self image];
//    image3.image = [self image];
//    
//    [self start];
//}

-(UIImage*)image{
    int r = [VknUtils randInt:10];
    if(currnt == r){
        r++;
        if(r > 10){
            r = 0;
        }
    }
    currnt = r;
    NSLog(@"r:%d", r);
    NSString* imageName = [NSString stringWithFormat:@"splash%d", r];
    return [UIImage imageNamed:imageName];
}

@end
