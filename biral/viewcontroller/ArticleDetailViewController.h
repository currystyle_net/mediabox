//
//  ArticleDetailViewController.h
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "BaseViewController.h"

@protocol ArticleDetailViewDelegate <NSObject>

@end

@interface ArticleDetailViewController : BaseViewController

-(id) initWithDelegate:(NSObject<ArticleDetailViewDelegate>*)delegate_;
-(void) load:(Article*)article;
-(void) die;

@end
