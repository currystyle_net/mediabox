//
//  ArticleListViewController.m
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "ArticleListViewController.h"
#import "ArticleCell.h"
#import "Visual.h"

#define AD_PER_PAGE 10

@interface ArticleListViewController ()<
ArticleCellDelegate
, VisualDelegate
, UITableViewDataSource
, UITableViewDelegate
>{
    __weak NSObject<ArticleListViewDelegate>* delegate;
    UITableView* tableView;
    UIView* tableWrap;
    ArticleList* articleList;
    Site* site;
    Visual* visual;
    UIView* headerLine;
    UIView* footerLine;
}

@end

@implementation ArticleListViewController

-(id) initWithDelegate:(NSObject<ArticleListViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    self.view.backgroundColor = [UIColor blackColor];
//    [self setHeader];
    [self setVisual];
    [self setTableView];
    
    // line
//    headerLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
//    headerLine.alpha = 0.3f;
//    footerLine = [[UIView alloc] initWithFrame:CGRectMake(0, [VknDeviceUtil windowHeight]-50-26, 320, 26)];
//    footerLine.alpha = 0.3f;
//    
//    [self.view addSubview:headerLine];
//    [self.view addSubview:footerLine];
}

-(void)setVisual{
    visual = [Visual createWithDelegate:self];
    [self.view addSubview:[visual getView]];
}

-(void) onSelectVisual:(Article *)article{
    [delegate onSelectArticle:article];
    [VknUtils wait:0.3f callback:^{
        [tableView reloadData];
    }];
}

-(void) setTableView{
    tableWrap = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 210, 320)];
    tableWrap.transform = CGAffineTransformMakeRotation(M_PI * -90.0f / 180.0f);
    tableWrap.center = CGPointMake(160, [VknDeviceUtil windowHeight]-105.0f-50.0f-26.5f);
    
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 210, 320)];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.delaysContentTouches = NO;
    tableView.contentOffset = CGPointMake(0, 1);
    tableView.backgroundColor = [UIColor clearColor];
    
    [tableWrap addSubview:tableView];
    [self.view addSubview:tableWrap];
}

-(void) reload:(Site*)site_{
    site = site_;
    articleList = site.articleList;
    [tableView reloadData];
    [visual reload:site];
    
    headerLine.backgroundColor = site.color;
    footerLine.backgroundColor = site.color;
}

#pragma mark -------- UITableViewDelegate -----------
#pragma mark -------- UITableViewDatasource -----------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int c = [articleList count];
    c = c + (c/AD_PER_PAGE);
    return c;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellIdentifier = @"articleCell";
    
    ArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[ArticleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setDelegate:self];
    }
    
    Article* article = ([self isAdIndex:indexPath]) ? [Ad get] : [self model:indexPath];
    [cell reload:article];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 133;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ArticleCell* cell = (ArticleCell*)[tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [cell light];
//    [BiralUtil pain:cell];
    
    [VknThreadUtil mainBlock:^{
        [delegate onSelectArticle:[cell article]];
    }];
    [VknUtils wait:0.2f callback:^{
        [tableView reloadData];
    }];
}

-(BOOL) isAdIndex:(NSIndexPath*)indexPath{
    if(![BiralUtil isJaDevice]){
        return NO;
    }
    int row = indexPath.row;
    return (row != 0 && row % AD_PER_PAGE == 0 );
}


-(int) modelIndex:(NSIndexPath*)indexPath{
    if(![BiralUtil isJaDevice]){
        return indexPath.row;
    }
    
    int row = indexPath.row;
    int modelIndex = row - (row / AD_PER_PAGE);
    
    return modelIndex;
}

-(Article*)model:(NSIndexPath*)indexPath{
    return [articleList get:[self modelIndex:indexPath]];
}


#pragma mark -------- UISCrolLViewDelegate -----------

-(void) scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [BiralUtil mainSv].scrollEnabled = NO;
}

-(void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(!decelerate){
        [BiralUtil mainSv].scrollEnabled = YES;
    }
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [BiralUtil mainSv].scrollEnabled = YES;
    
    float sa = scrollView.contentSize.height - (scrollView.frame.size.height + scrollView.contentOffset.y);
    if(scrollView.contentOffset.y == 0){
        [scrollView setContentOffset:CGPointMake(0, 1.0f) animated:YES];
    }else if(sa == 0){
        [scrollView setContentOffset:CGPointMake(0, scrollView.contentOffset.y-1) animated:YES];
    }
}

@end
