//
//  AdArticleViewController.m
//  biral
//
//  Created by kawase yu on 2014/07/26.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "AdArticleViewController.h"
#import "VknURLLoadCommand.h"

@interface AdArticleViewController (){
    NSObject<AdArticleViewDelegate>* delegate;
    UILabel* titleLabel;
    UILabel* detailLabel;
    UILabel* descriptionLabel;
    UILabel* ratingLabel;
    UIButton* linkButton;
    UIImageView* imageView;
    UIScrollView* sv;
    AdArticle* article;
    
    UIScrollView* screenshotSv;
}

@end

@implementation AdArticleViewController

-(id) initWithDelegate:(NSObject<AdArticleViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    self.view.backgroundColor = [UIColor whiteColor];
    [self setContents];
    
    // statusBar
    UIView* bar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    bar.backgroundColor = [UIColor blackColor];
    bar.alpha = 0.2f;
    [self.view addSubview:bar];
    
    // event
    UISwipeGestureRecognizer* reco = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(tapBack)];
    reco.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:reco];
    
    // back
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(260, [VknDeviceUtil windowHeight] - 105, 45, 45);
    [backButton setImage:[UIImage imageNamed:@"adBackButton"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(tapBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

-(void) setContents{
    sv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]-50)];
    [self.view addSubview:sv];
    
    imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(60, 20, 200, 101);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [sv addSubview:imageView];
    
    titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [sv addSubview:titleLabel];
    
    ratingLabel = [[UILabel alloc] init];
    ratingLabel.font = [UIFont systemFontOfSize:13.0f];
    ratingLabel.numberOfLines = 2;
    [sv addSubview:ratingLabel];
    
    detailLabel = [[UILabel alloc] init];
    detailLabel.numberOfLines = 0;
    [sv addSubview:detailLabel];
    
    descriptionLabel = [[UILabel alloc] init];
    descriptionLabel.numberOfLines = 0;
    [sv addSubview:descriptionLabel];
    
    linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [linkButton setImage:[UIImage imageNamed:@"download"] forState:UIControlStateNormal];
    [linkButton setImage:[UIImage imageNamed:@"downloadOn"] forState:UIControlStateHighlighted];
    [linkButton addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
    [sv addSubview:linkButton];
    
    screenshotSv = [[UIScrollView alloc] init];
    [sv addSubview:screenshotSv];
}



-(void) tapButton{
    [VknUtils openBrowser:article.linkUrl];
}

-(void) tapBack{
    [[BiralUtil pageDelegate] showOverlay];
    [BiralUtil mainSv].scrollEnabled = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) load:(AdArticle*)article_{
    article = article_;
    float y = 20 + 101 + 20;
    
    titleLabel.frame = CGRectMake(10, y, 300, 10000);
    titleLabel.text = article.title;
    [titleLabel sizeToFit];
    CGRect titleFrame = titleLabel.frame;
    if(titleFrame.size.width < 300){
        titleFrame.size.width = 300;
        titleLabel.frame = titleFrame;
    }
    y += titleLabel.frame.size.height + 15;
    
    ratingLabel.frame = CGRectMake(10, y, 300, 40);
    y += 40+15;
    
    screenshotSv.frame = CGRectMake(0, y, 320, 372);
    screenshotSv.backgroundColor = UIColorFromHex(0xeeeeee);
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(-200, 0, 1000, 0.5f)];
    line.backgroundColor = UIColorFromHex(0xcccccc);
    [screenshotSv addSubview:line];
    line = [[UIView alloc] initWithFrame:CGRectMake(-200, 371.5f, 1000, 0.5f)];
    line.backgroundColor = UIColorFromHex(0xcccccc);
    [screenshotSv addSubview:line];
    
    y += 372 + 15;
    
    descriptionLabel.frame = CGRectMake(10, y, 300, 10000);
    descriptionLabel.text = article.description;
    [descriptionLabel sizeToFit];
    y += descriptionLabel.frame.size.height + 15;
    
    detailLabel.frame = CGRectMake(10, y, 300, 10000);
    detailLabel.text = article.detail;
    [detailLabel sizeToFit];
    y += detailLabel.frame.size.height + 15;
    
    linkButton.frame = CGRectMake(50, y, 220, 44);
    y += linkButton.frame.size.height + 15 + 10 + 45;
    
    sv.contentSize = CGSizeMake(320, y);
    
    [VknImageCache loadImage:article.imageUrl defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        if(![key isEqualToString:article.imageUrl]) return;
        [VknThreadUtil mainBlock:^{
            imageView.image = image;
        }];
    }];
    
    [self loadApi];
}

-(void) loadApi{
    NSString* api = [NSString stringWithFormat:@"http://itunes.apple.com/lookup?country=JP&id=%@", article.appId];
    NSLog(@"%@", api);
    VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:api];
    c.onComplete = ^(NSData* data){
        NSDictionary* jsonDic = [VknDataUtil dataToJson:data];
        if(jsonDic == nil) return;
        NSLog(@"%@", jsonDic);
        NSArray* results = [jsonDic objectForKey:@"results"];
        if(results == nil) return;
        if([results count] == 0) return;
        NSDictionary* result = [results firstObject];
        NSArray* urls = [result objectForKey:@"screenshotUrls"];
        float rating = [[result objectForKey:@"averageUserRatingForCurrentVersion"] floatValue];
        int ratingCount = [[result objectForKey:@"userRatingCountForCurrentVersion"] intValue];
        NSString* version = [result objectForKey:@"version"];
        
        ratingLabel.text = [NSString stringWithFormat:@"【評価：%d】 \n%d件の評価(バージョン%@)", (int)rating, ratingCount, version];
        for(int i=0,max=[urls count];i<max;i++){
            NSString* url = [urls objectAtIndex:i];
            [self loadScreenshot:url index:i];
        }
        
        screenshotSv.contentSize = CGSizeMake(([urls count] * 210) + 10, 372);
    };
    
    [c execute:nil];
}

-(void) loadScreenshot:(NSString*)url index:(int)index{
    CGRect frame = CGRectMake(index * 210+10, 10, 200, 352);
    UIImageView* ssView = [[UIImageView alloc] initWithFrame:frame];
    [screenshotSv addSubview:ssView];
    [VknImageCache loadImage:url defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        [VknThreadUtil mainBlock:^{
            ssView.image = image;
        }];
    }];
}

-(void) adjustPosition{
    
}

@end
