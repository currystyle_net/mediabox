//
//  ArticleListViewController.h
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "BaseViewController.h"

@protocol ArticleListViewDelegate <NSObject>

-(void) onSelectArticle:(Article*)article;

@end

@interface ArticleListViewController : BaseViewController

-(id) initWithDelegate:(NSObject<ArticleListViewDelegate>*)delegate_;
-(void) reload:(Site*)site_;

@end
