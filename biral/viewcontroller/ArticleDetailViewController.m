//
//  ArticleDetailViewController.m
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "ArticleDetailViewController.h"
#import <Social/Social.h>
#import "VknPostCommand.h"
#import "Api.h"

@interface ArticleDetailViewController ()<
UIWebViewDelegate
, UIScrollViewDelegate>{
    __weak Article* article;
    __weak NSObject<ArticleDetailViewDelegate>* delegate;
    UILabel* titleLabel;
    UIView* headerView;
    UIWebView* webView;
    UIView* menuView;
    
    UIButton* backButton;
    UIButton* nextButton;
    UIView* shareView;
    UIView* overlay;
    int currntPoint;
    BOOL isInitialLoad;
}

@end

@implementation ArticleDetailViewController

-(id) initWithDelegate:(NSObject<ArticleDetailViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    self.view.backgroundColor = [UIColor whiteColor];
    
    // views
    [self setWebView];
    [self setMenuView];
    [self setShareView];
    
    // statusbar
    UIView* statusBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    statusBar.backgroundColor = [UIColor blackColor];
    statusBar.alpha = 0.2f;
    [self.view addSubview:statusBar];
    
    // event
    UISwipeGestureRecognizer* reco = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(tapBack)];
    reco.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:reco];
}

-(void) tapBack{
    [[BiralUtil pageDelegate] showOverlay];
    [BiralUtil mainSv].scrollEnabled = YES;
    [self die];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setWebView{
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]-50)];
    webView.delegate = self;
    webView.scrollView.delegate = self;
    webView.scrollView.contentInset = UIEdgeInsetsMake(/*20*/0, 0, 45, 0);
    webView.backgroundColor = [UIColor blackColor];
    
    [self.view addSubview:webView];
}

-(void) setMenuView{
    menuView = [[UIView alloc] initWithFrame:CGRectMake(0, [VknDeviceUtil windowHeight]-45-50, 320, 45)];
    [self.view addSubview:menuView];
    
    UIToolbar* toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
    [menuView addSubview:toolbar];
    
//    128 × 89
    CGRect buttonFrame = CGRectMake(0, 0, 64, 44.5f);
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = buttonFrame;
    [backButton setImage:[UIImage imageNamed:@"webtabBack"] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"webtabBackOff"] forState:UIControlStateDisabled];
    [backButton addTarget:self action:@selector(webBack) forControlEvents:UIControlEventTouchUpInside];
    backButton.enabled = NO;
    buttonFrame.origin.x += 64;
    [menuView addSubview:backButton];
    
    nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = buttonFrame;
    [nextButton setImage:[UIImage imageNamed:@"webtabNext"] forState:UIControlStateNormal];
    [nextButton setImage:[UIImage imageNamed:@"webtabNextOff"] forState:UIControlStateDisabled];
    [nextButton addTarget:self action:@selector(webNext) forControlEvents:UIControlEventTouchUpInside];
    nextButton.enabled = NO;
    buttonFrame.origin.x += 64;
    [menuView addSubview:nextButton];
    
    UIButton* reloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    reloadButton.frame = buttonFrame;
    [reloadButton setImage:[UIImage imageNamed:@"webtabReload"] forState:UIControlStateNormal];
    [reloadButton addTarget:self action:@selector(webReload) forControlEvents:UIControlEventTouchUpInside];
    buttonFrame.origin.x += 64;
    [menuView addSubview:reloadButton];
    
    UIButton* shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    shareButton.frame = buttonFrame;
    [shareButton setImage:[UIImage imageNamed:@"webtabShare"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(webShare) forControlEvents:UIControlEventTouchUpInside];
    buttonFrame.origin.x += 64;
    [menuView addSubview:shareButton];
    
    UIButton* pageBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    pageBackButton.frame = buttonFrame;
    [pageBackButton setImage:[UIImage imageNamed:@"webtabPageback"] forState:UIControlStateNormal];
    [pageBackButton addTarget:self action:@selector(tapBack) forControlEvents:UIControlEventTouchUpInside];
    buttonFrame.origin.x += 64;
    [menuView addSubview:pageBackButton];
    
//    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
//    line.backgroundColor = UIColorFromHex(0x777777);
//    [menuView addSubview:line];
}

-(void) setShareView{
    shareView = [[UIView alloc] initWithFrame:CGRectMake(0, [VknDeviceUtil windowHeight], 320, 393)];
    UIView* bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 393)];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.alpha = 0.95f;
    [shareView addSubview:bgView];
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 48.5f)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"シェアする";
    [shareView addSubview:label];
    
    CGRect buttonFrame = CGRectMake(0, 48.5f, 320, 44.5f);
    UIButton* twButton = [UIButton buttonWithType:UIButtonTypeCustom];
    twButton.frame = buttonFrame;
    [twButton setImage:[UIImage imageNamed:@"twButton"] forState:UIControlStateNormal];
    [twButton addTarget:self action:@selector(tapTw) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:twButton];
    buttonFrame.origin.y += buttonFrame.size.height;
    
    UIButton* fbButton = [UIButton buttonWithType:UIButtonTypeCustom];
    fbButton.frame = buttonFrame;
    [fbButton setImage:[UIImage imageNamed:@"fbButton"] forState:UIControlStateNormal];
    [fbButton addTarget:self action:@selector(tapFb) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:fbButton];
    buttonFrame.origin.y += buttonFrame.size.height;
    
    UIButton* lineButton = [UIButton buttonWithType:UIButtonTypeCustom];
    lineButton.frame = buttonFrame;
    [lineButton setImage:[UIImage imageNamed:@"lineButton"] forState:UIControlStateNormal];
    [lineButton addTarget:self action:@selector(tapLine) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:lineButton];
    buttonFrame.origin.y += buttonFrame.size.height;
    
    UIButton* safariButton = [UIButton buttonWithType:UIButtonTypeCustom];
    safariButton.frame = buttonFrame;
    [safariButton setImage:[UIImage imageNamed:@"safariButton"] forState:UIControlStateNormal];
    [safariButton addTarget:self action:@selector(tapSafari) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:safariButton];
    buttonFrame.origin.y += buttonFrame.size.height;
    
    UIButton* urlButton = [UIButton buttonWithType:UIButtonTypeCustom];
    urlButton.frame = buttonFrame;
    [urlButton setImage:[UIImage imageNamed:@"urlButton"] forState:UIControlStateNormal];
    [urlButton addTarget:self action:@selector(tapUrl) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:urlButton];
    buttonFrame.origin.y += buttonFrame.size.height;
    
    UIButton* ihanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    ihanButton.frame = buttonFrame;
    [ihanButton setImage:[UIImage imageNamed:@"ihanButton"] forState:UIControlStateNormal];
    [ihanButton addTarget:self action:@selector(tapIhan) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:ihanButton];
    buttonFrame.origin.y += buttonFrame.size.height;
    
    UIButton* cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    440 × 80
    cancelButton.frame = CGRectMake(50, buttonFrame.origin.y + 15, 220, 40);
    [cancelButton setImage:[UIImage imageNamed:@"cancelButton"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(tapCancel) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:cancelButton];
    
    overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight])];
    overlay.backgroundColor = [UIColor blackColor];
    overlay.alpha = 0;
    overlay.userInteractionEnabled = NO;
    [overlay addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCancel)]];
    [self.view addSubview:overlay];
    
    [self.view addSubview:shareView];
}

-(void) showMenu{
    [UIView animateWithDuration:0.2f animations:^{
        menuView.transform = CGAffineTransformMakeTranslation(0, 0);
    } completion:^(BOOL finished) {
        
    }];
}

-(void) hideMenu{
    [UIView animateWithDuration:0.2f animations:^{
        menuView.transform = CGAffineTransformMakeTranslation(0, 45);
    } completion:^(BOOL finished) {
        
    }];
}

-(void) tapCancel{
    overlay.userInteractionEnabled = NO;
    [UIView animateWithDuration:0.3f animations:^{
        shareView.transform = CGAffineTransformMakeTranslation(0, 0);
        overlay.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) tapTw{
    [BiralUtil shareTw:article];
//    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
//    NSDictionary *params = @{@"status" : [NSString stringWithFormat:@"%@\n%@"
//                                          , [self thisTitle]
//                                          , [self thisUrl]]};
//    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
//                                            requestMethod:SLRequestMethodPOST
//                                                      URL:url
//                                               parameters:params];
//    
//    
//    __weak ArticleDetailViewController* SELF = self;
//    [BiralUtil currntTwAccount:^(ACAccount *account) {
//        if(account == nil){
//            UIAlertView* alertView = [[UIAlertView alloc] init];
//            alertView.title = @"アカウントが取得できませんでした";
//            alertView.message = @"端末の「設定」→「プライバシー」よりTwitterアカウントの利用を許可してください";
//            [alertView addButtonWithTitle:@"OK"];
//            [alertView show];
//            return;
//        }
//        [SELF showLoading];
//        [request setAccount:account];       // 投稿アカウントの指定
//        [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
//            [VknThreadUtil mainBlock:^{
//                [SELF hideLoading];
//                [self tapCancel];
//                [self showToast:@"投稿しました"];
//            }];
//        }];
//    }];
}

-(void) tapIhan{
    VknPostCommand* c = [[VknPostCommand alloc] initWithValues:@{@"id": article.linkUrl} withUrl:[Api url:@"ihan.php"]];
    c.onComplete = ^(NSData* data){
        
    };
    c.onFail = ^(NSError* error){
        
    };
    [c execute:nil];
    
    NSLog(@"sendIhan");
    
    [self showToast:@"報告しました\nご協力ありがとうございます"];
}

-(NSString*)thisTitle{
    return [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}

-(NSString*)thisUrl{
    return [webView stringByEvaluatingJavaScriptFromString:@"document.URL"];
}

-(void) tapFb{
    [BiralUtil shareFb:article];
}

-(void) tapLine{
    [BiralUtil shareLine:article];
}

-(void) tapSafari{
    [VknUtils openBrowser:article.linkUrl];
}

-(void) tapUrl{
    UIPasteboard *board = [UIPasteboard generalPasteboard];
    [board setValue:[self thisUrl] forPasteboardType:@"public.utf8-plain-text"];
    
    [self showToast:@"コピーしました"];
    [self tapCancel];
}

-(void) webBack{
    if([webView canGoBack]){
        [webView goBack];
        [self showLoading];
    }
    backButton.enabled = NO;
}

-(void) webNext{
    if([webView canGoForward]){
        [webView goForward];
        [self showLoading];
    }
    nextButton.enabled = NO;
}

-(void) webReload{
    [self showLoading];
    [webView reload];
}

-(void) webShare{
    overlay.userInteractionEnabled = YES;
    [UIView animateWithDuration:0.3f animations:^{
        shareView.transform = CGAffineTransformMakeTranslation(0, -393-89);
        overlay.alpha = 0.15f;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) load:(Article*)article_{
    article = article_;
    isInitialLoad = YES;
    headerView.backgroundColor = [article bgColor];
    titleLabel.text = article.site.name;
    [self showLoading];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:article.linkUrl]]];
    
    webView.transform = CGAffineTransformMakeScale(0.95f, 0.95f);
    webView.alpha = 0;
}

-(void) webViewDidFinishLoad:(UIWebView *)webView{
    [self hideLoading];
    backButton.enabled = [webView canGoBack];
    nextButton.enabled = [webView canGoForward];
    
    if(isInitialLoad){
        isInitialLoad = NO;
        [UIView animateWithDuration:0.2f delay:0.2f options:UIViewAnimationOptionCurveEaseIn animations:^{
            webView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
            webView.alpha = 1.0f;
        } completion:^(BOOL finished) {
            
        }];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked ){
        [self showLoading];
    }
    
    return YES;
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView{
    float y = scrollView.contentOffset.y;
    
    if(y > currntPoint){
        if(y > 5)
            [self hideMenu];
    }else{
        [self showMenu];
    }
    
    currntPoint = y;
}

-(void) die{
    webView.delegate = nil;
}

-(void) dealloc{
//    NSLog(@"dealloc");
    [webView removeFromSuperview];
    webView.delegate = nil;
    webView = nil;
}

@end
