//
//  Article.m
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "Article.h"

@interface Article (){
    NSArray* categories;
}

@end

@implementation Article

@synthesize siteId;
@synthesize imageUrl;
@synthesize title;
@synthesize description;
@synthesize linkUrl;
@synthesize published;
@synthesize isShow;
@synthesize site;
@synthesize readed;

-(id) initWithDictionary:(NSDictionary *)dic{
    self = [super initWithDictionary:dic];
    if(self){
        siteId = [[dic objectForKey:@"site_id"] intValue];
        imageUrl = [dic objectForKey:@"image_url"];
        title = [dic objectForKey:@"title"];
        description = [dic objectForKey:@"description"];
        linkUrl = [dic objectForKey:@"link_url"];
        published = [NSDate dateWithTimeIntervalSince1970:[[dic objectForKey:@"published_at"] intValue]];
        isShow = ([[dic objectForKey:@"is_show"] intValue] == 1);
        categories = [VknDataUtil strToJsonArray:[dic objectForKey:@"category"]];
        
        readed = [BiralUtil isReaded:id_];
    }
    
    return self;
}

-(NSString*)category{
    if([categories count] == 0) return @"";
    return [categories objectAtIndex:0];
}

-(BOOL) hasImage{
    return ([imageUrl length] != 0);
}

-(UIColor*)bgColor{
    switch (siteId) {
        case 1:
            return UIColorFromHex(0xffcccc);
        case 2:
            return UIColorFromHex(0xffffcc);
        case 3:
            return UIColorFromHex(0xccccff);
        case 4:
            return UIColorFromHex(0xffccff);
        case 5:
            return UIColorFromHex(0xccffff);
        case 6:
            return UIColorFromHex(0xccffcc);
        case 7:
            return UIColorFromHex(0xcccccc);
    }
    
    return [UIColor whiteColor];
}

-(BOOL)isAd{
    return NO;
}

@end
