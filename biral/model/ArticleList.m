//
//  ArticleList.m
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "ArticleList.h"

@implementation ArticleList

-(id) initWithArray:(NSArray*)array{
    self = [super init];
    if(self){
        for(NSDictionary* dic in array){
            Article* article = [[Article alloc] initWithDictionary:dic];
            [self add:article];
        }
    }
    
    return self;
}

-(Article*)get:(int)index{
    return (Article*)[super get:index];
}

-(ArticleList*)visualList{
    ArticleList* list = [[ArticleList alloc] init];
    for(int i=0,max=[self count];i<max;i++){
        Article* article = [self get:i];
        if(![article hasImage]) continue;
        [list add:article];
    }
    
    return list;
}

@end
