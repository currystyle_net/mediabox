//
//  AdArticle.h
//  biral
//
//  Created by kawase yu on 2014/07/26.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "Article.h"
#import "ChkRecordData.h"

@interface AdArticle : Article

-(id) initWithChkdata:(ChkRecordData*)data;
+(AdArticle*)createKaimono;
+(AdArticle*)createSeiji;
+(AdArticle*)createYakuzai;

@property NSString* detail;
@property NSString* appId;

@end
