//
//  SiteList.m
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "SiteList.h"

@implementation SiteList

-(id) initWithArray:(NSArray*)array{
    self = [super init];
    if(self){
        for(NSDictionary* dic in array){
            Site* site = [[Site alloc] initWithDictionary:dic];
            [self add:site];
        }
    }
    
    return self;
}

-(Site*)get:(int)index{
    return (Site*)[super get:index];
}

@end
