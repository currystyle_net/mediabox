//
//  Article.h
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "VknModel.h"
#import "Site.h"

@interface Article : VknModel

@property int siteId;
@property NSString* imageUrl;
@property NSString* title;
@property NSString* description;
@property NSString* linkUrl;
@property NSDate* published;
@property BOOL isShow;
@property Site* site;
@property BOOL readed;

-(UIColor*)bgColor;
-(BOOL) hasImage;
-(NSString*)category;
-(BOOL) isAd;

@end
