//
//  Site.h
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "VknModel.h"
#import "ArticleList.h"

@class ArticleList;

@interface Site : VknModel

@property NSString* name;
@property NSString* url;
@property NSString* rssUrl;
@property BOOL isShow;
@property ArticleList* articleList;

-(UIColor*)color;

@end
