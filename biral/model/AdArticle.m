//
//  AdArticle.m
//  biral
//
//  Created by kawase yu on 2014/07/26.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "AdArticle.h"

@implementation AdArticle

//@property (nonatomic, retain, readonly) NSString  *articleId;
//@property (nonatomic, retain, readonly) NSString  *appId;
//@property (nonatomic, retain, readonly) NSString  *title;
//@property (nonatomic, retain, readonly) NSString  *description;
//@property (nonatomic, retain, readonly) NSString  *detail;
//@property (nonatomic, retain, readonly) NSString  *cvCondition;
//@property (nonatomic, retain, readonly) NSString  *linkUrl;
//@property (nonatomic, retain, readonly) NSString  *type;
//@property (nonatomic, retain, readonly) NSString  *imageIcon;
//@property (nonatomic, retain, readonly) NSString  *appSearcher;
//@property (nonatomic, retain, readonly) NSNumber  *price;
//@property (nonatomic, retain, readonly) NSNumber  *oldPrice;
//@property (nonatomic, retain, readonly) NSNumber  *reward;
//@property (nonatomic, retain, readonly) NSNumber  *point;
//@property (nonatomic, retain, readonly) NSNumber  *categoryId;
//@property (nonatomic, retain, readonly) NSString  *category;
//@property (nonatomic, retain, readonly) NSString  *impUrl;
//@property (nonatomic, retain, readonly) NSString  *appStoreId;
//
//@property (nonatomic, readonly)         BOOL      isInstalled;
//@property (nonatomic, readonly)         BOOL      isImpSend;
//
//@property (nonatomic, retain)           UIImage   *cacheImage;
//@property (nonatomic, retain)           UIImage   *cashImage UNAVAILABLE_ATTRIBUTE __attribute__((unavailable("This property is unavailable!")));
//@property (nonatomic, retain, readonly) NSNumber  *conversionActionType;
//@property (nonatomic, readonly) BOOL              isMeasuring;
//
//@property (nonatomic, retain, readonly) NSString  *nativeBannerUrl;
//@property (nonatomic, readonly)         BOOL      hasNativeBanner;


@synthesize siteId;
@synthesize imageUrl;
@synthesize title;
@synthesize description;
@synthesize linkUrl;
@synthesize published;
@synthesize isShow;
@synthesize site;
@synthesize detail;
@synthesize appId;

-(id) initWithChkdata:(ChkRecordData*)data{
    self = [super init];
    if(self){
        imageUrl = data.imageIcon;
        title = [NSString stringWithFormat:@"【PR】%@", data.title];
        description = data.description;
        detail = data.detail;
        linkUrl = data.linkUrl;
        appId = data.appStoreId;
        if(data.hasNativeBanner){
            imageUrl = data.nativeBannerUrl;
        }
    }
    
    return self;
}

-(BOOL)isAd{ return YES; }

+(AdArticle*)createKaimono{
    AdArticle* article = [[AdArticle alloc] init];
    article.title = @"お買い物メモ ～手書き風の可愛いメモ帳～";
    article.description = @"買い忘れがもう無くなる！？\n"
    "メモをとらないと買うものを忘れてしまう。でもメモをとるのが面倒くさい。\n"
    "スーパーに着いてから買うものを考えていると、ついつい無駄なもの迄買ってしまいがち。\n"
    "お買い物メモを使えば、そんな手間や失敗がなくなるかも！？\n";
    article.detail = @"■□■ 特徴 ■□■\n"
    "\n"
    "・アイテムリストから商品を選ぶだけで簡単に買物リストを作成可能。\n"
    "・お店のコーナー毎や、買いたい順にリストを並び替えることが可能！\n"
    "・本当のメモ帳にメモをとっているような手書き風の可愛いアイコンで、商品をカテゴリー毎に分けて表示。\n"
    "・【新機能】可愛いアイコンを多数用意！好きなアイコンを使用して自分だけのメモ帳を作成可能。\n"
    "・履歴が分かりやすく表示された電卓を内蔵。割引計算も簡単にする事が可能。\n"
    "・お気に入り機能を実装し、買い物リストをブックマークする事が可能。\n"
    "・メール機能を実装し、買ってきてほしい商品を選ぶだけで簡単にメールを送る事が可能。\n"
    "・【新機能】カラーテーマを変更可能。その日の気分に合わせてカラーを変えてみては！？\n"
    "・【新機能】画面下の広告を削除するアドオンを搭載。\n"
    "\n"
    "■□■ 使い方 ■□■\n"
    "\n"
    "アイテムリストから買う予定の商品を選びます。\n"
    "たったこれだけでお買い物リストに商品が登録されます。\n"
    "後は実際にお買い物に行き、買い物カゴに入れた商品からチェックをしていくだけ！\n"
    "誰でも直感的に操作をする事が可能です。\n"
    "\n"
    "このアプリを使っていただき、主婦の皆様にメモをとる手間や、買い忘れが少しでも減っていただければうれしいです。\n";
    article.linkUrl = @"https://itunes.apple.com/jp/app/id607634035?mt=8";
    article.imageUrl = @"http://a4.mzstatic.com/us/r30/Purple5/v4/ed/0c/82/ed0c820b-f5b5-3dc7-c06f-3c4cc9ec494e/mzl.dyaghjlz.175x175-75.jpg";
    
    return article;
}

+(AdArticle*)createSeiji{
    AdArticle* article = [[AdArticle alloc] init];
    article.title = @"世論速報!!みんなの政治・経済";
    article.description = @"Twetter（ツイッター）と連動してネット世論、今話題の政治・経済関連の情報ニュース、政治経済のまとめニュースをお届けいたします。!\n"
    "\n"
    "ネット選挙が話題を集める今、楽しくスマートに最新の政治、経済のニュースを読むことで、快適に今の若者に政治・選挙を身近に感じてもらうための無料newsアプリです。";
    article.detail = @"■□■ 特徴 ■□■\n"
    "トップ画面に流れる様々なhashtag（ハッシュタグ）が、文字の大きさに変化を与えることにより今盛り上がっている話題が一目で感覚的にわかります。気になるハッシュタグをタップすることで、みんなの様々なつぶやきをタイムラインで表示します。盛り上がっている話題は、沢山のつぶやきが流れて表示されて多くの情報を収集することが可能です。\n"
    "\n"
    "またつぶやき以外にも、政治ニュース、経済ニュース、政治・経済のまとめニュースなど様々なコンテンツを、見やすくわかりやすい画面でお届けします。\n"
    "これを読むことにより政経の用語、読解力、また国際社会の問題など様々な問題に興味を持つ事ができるようになるかもしれません。\n"
    "naver（ネイバー）のまとめニュースもお届けします。ネイバーまとめから芸能、エンタメやスポーツ、サッカーやプロ野球などのまとめニュースも読む事が可能です。\n"
    "政治、経済の専門のニュースの情報収集にはこの無料ニュースアプリ、世論速報!!みんなの政治・経済をおすすめします！\n"
    "\n"
    "■□■ 解説 ■□■\n"
    "『世論速報！！みんなの政治・経済』アプリは大きく分けて5つの主となるコンテンツで構成されています。左から『トップ』、『ランキング』、『政治ニュース』、『経済ニュース』、『政治・経済まとめ』になります。\n"
    "画面を横にスワイプさせることにより、これらのページへ直感的に遷移することが可能です。\n"
    "デザインも洗礼されていて、今流行のフラットデザインを採用しsmartnewsな画面構成となっております。\n"
    "\n"
    "※Twetterアカウントを設定していただくことにより、タイムラインを表示することが可能になります";
    article.linkUrl = @"https://itunes.apple.com/us/app/shi-lun-su-bao!!minnano-zheng/id668321251?l=ja&ls=1&mt=8";
    article.imageUrl = @"http://a4.mzstatic.com/us/r30/Purple/v4/10/1e/16/101e164f-e26f-c61f-c1c7-cc2a6ced76d0/mzl.ihjcsosn.175x175-75.jpg";
    
    return article;
}

+(AdArticle*)createYakuzai{
    AdArticle* article = [[AdArticle alloc] init];
    article.title = @"薬剤師年収診断アプリ 『薬剤師年収処方箋』";
    article.description = @"わたしの年収って高い？安い？？\n"
    "人には聞けない・・・でも気になる！！\n"
    "そんな薬剤師さんのお悩みには「薬剤師年収処方箋」を！";
    
    article.detail = @"年齢やお住まいの地域、勤務体制で簡単にあなたの適正年収を算出します。\n"
    "※給与が少ない・・・\n"
    "※勤務時間が長い・・・\n"
    "※人間関係がつらい・・・\n"
    "そんなお悩みで転職を考えてるあなたにはオススメの求人や転職情報をお役立てください。\n"
    "\n"
    "その他、最新メディカル情報もチェック出来るのでお役立ちNo.1！！\n"
    "\n"
    "【ご注意】\n"
    "診断結果の適正給与数値結果は、確約をするものではありませんのでご注意ください。";
    article.linkUrl = @"https://itunes.apple.com/us/app/yao-ji-shi-nian-shou-zhen/id899049413?l=ja&ls=1&mt=8";
    article.imageUrl = @"http://a5.mzstatic.com/us/r30/Purple4/v4/06/e3/0e/06e30eaf-45f2-692b-75b5-8870a23b89fc/mzl.wzukrajm.175x175-75.jpg";
    
    return article;
}

-(NSString*)category{
    return @"【PR】";
}

-(BOOL) hasImage{
    return YES;
}

@end
