//
//  SiteList.h
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "VknModelList.h"
#import "Site.h"

@interface SiteList : VknModelList

-(id) initWithArray:(NSArray*)array;
-(Site*)get:(int)index;

@end
