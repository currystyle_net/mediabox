//
//  ArticleList.h
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "VknModelList.h"
#import "Article.h"

@interface ArticleList : VknModelList

-(id) initWithArray:(NSArray*)array;
-(Article*)get:(int)index;
-(ArticleList*)visualList;

@end
