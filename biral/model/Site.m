//
//  Site.m
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "Site.h"

@implementation Site

@synthesize name;
@synthesize url;
@synthesize rssUrl;
@synthesize isShow;
@synthesize articleList;

-(id) initWithDictionary:(NSDictionary *)dic{
    self = [super initWithDictionary:dic];
    if(self){
        name = [dic objectForKey:@"name"];
        url = [dic objectForKey:@"url"];
        rssUrl = [dic objectForKey:@"rss_url"];
        isShow = ([[dic objectForKey:@"is_show"] intValue] == 1);
    }
    
    return self;
}

-(UIColor*)color{
    switch (id_) {
        case 1: return UIColorFromHex(0xff9999);
        case 2: return UIColorFromHex(0xffff99);
        case 3: return UIColorFromHex(0x9999ff);
        case 4: return UIColorFromHex(0x99ffff);
        case 5: return UIColorFromHex(0x99ff99);
        case 6: return UIColorFromHex(0xf9999f);
        case 7: return UIColorFromHex(0x9ff999);
        case 8: return UIColorFromHex(0x99f99f);
        case 9: return UIColorFromHex(0xf9f9f9);
            
        default:
            return [UIColor grayColor];
    }
}

@end
