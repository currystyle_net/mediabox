//
//  PageDelegate.h
//  biral
//
//  Created by kawase yu on 2014/07/26.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PageDelegate <NSObject>

-(void) showOverlay;
-(void) hideOverlay;

@end