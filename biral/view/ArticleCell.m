//
//  ArticleCell.m
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "ArticleCell.h"

@interface ArticleCell (){
    __weak NSObject<ArticleCellDelegate>* delegate;
    __weak Article* article;
    
    UILabel* titleLabel;
    UILabel* categoryLabel;
    UILabel* descriptionLabel;
    UILabel* dateLabel;
    UILabel* siteLabel;
    UIImageView* imageView;
    UIView* nophotoView;
    UIImageView* nophotoBg;
    UILabel* siteNameLabel;
    UIActivityIndicatorView* indicator;
    UIImageView* kidoku;
    UIImageView* lightBg;
}

@end

@implementation ArticleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void) initialize{
//    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.transform = CGAffineTransformMakeRotation(M_PI * (90) / 180.0f);
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    UIView* wrapView = [[UIView alloc] initWithFrame:CGRectMake(3, 0, 132, 210)];
    [self.contentView addSubview:wrapView];
    
    UIImageView* bgImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cellBg"]];
    [wrapView addSubview:bgImage];
    
    lightBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellBgOn"]];
    lightBg.alpha = 0;
    [wrapView addSubview:lightBg];
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 20, 120, 90)];
    imageView.contentMode =  UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.backgroundColor = [UIColor grayColor];
    [wrapView addSubview:imageView];
    
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    indicator.frame = imageView.frame;
    [wrapView addSubview:indicator];
    
    nophotoView = [[UIView alloc] initWithFrame:imageView.frame];
    nophotoBg = [[UIImageView alloc] initWithImage:
    [UIImage imageNamed:[NSString stringWithFormat:@"nophoto%d",[VknUtils randIntRange:NSMakeRange(0, 7)]]]];
    nophotoBg.frame = CGRectMake(0, 0, 120, 90);
    nophotoBg.contentMode =  UIViewContentModeScaleAspectFill;
    nophotoBg.clipsToBounds = YES;
    [nophotoView addSubview:nophotoBg];
    siteLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 90)];
    siteLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    siteLabel.textAlignment = NSTextAlignmentCenter;
    siteLabel.textColor = [UIColor whiteColor];
    [nophotoView addSubview:siteLabel];
    [wrapView addSubview:nophotoView];
    
    categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 120, 10)];
    categoryLabel.font = [UIFont boldSystemFontOfSize:9.0f];
    categoryLabel.textColor = UIColorFromHex(0xAAAAAA);
    [wrapView addSubview:categoryLabel];
    
    titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 4;
    titleLabel.font = [UIFont boldSystemFontOfSize:11.0f];
    [wrapView addSubview:titleLabel];
    
    descriptionLabel = [[UILabel alloc] init];
    descriptionLabel.numberOfLines = 3;
    descriptionLabel.font = [UIFont systemFontOfSize:11.0f];
    [wrapView addSubview:descriptionLabel];

    kidoku = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listKidoku"]];
    kidoku.frame = CGRectMake(95, 2, 33, 33);
    [wrapView addSubview:kidoku];
}

-(void) setDelegate:(NSObject<ArticleCellDelegate>*)delegate_{
    delegate = delegate_;
}

-(void) reload:(Article*)article_{
    article = article_;
    
    titleLabel.frame = CGRectMake(5, 113, 120, 1000);
    titleLabel.text = article.title;
    [titleLabel sizeToFit];
    
    int titleHeight = titleLabel.frame.size.height;
    NSLog(@"%d", titleHeight);
    if(titleHeight <= 40){ // 3行
        descriptionLabel.numberOfLines = 4;
    }else if(titleHeight <= 53){ // 4行
        descriptionLabel.numberOfLines = 3;
    }else{ // 2行以下
        descriptionLabel.numberOfLines = 5;
    }
    
    descriptionLabel.frame = CGRectMake(5, 113+titleLabel.frame.size.height+2, 120, 10000);
    descriptionLabel.text = article.description;
    [descriptionLabel sizeToFit];
    
    categoryLabel.text = [article category];
    if([[article category] length] == 0){
        categoryLabel.text = article.site.name;
    }
    siteLabel.text = article.site.name;
    imageView.image = nil;
    nophotoView.hidden = [article hasImage];
    kidoku.hidden = !article.readed;
    
    if(![article hasImage]) return;
    
    [indicator startAnimating];
    imageView.alpha = 0;
    imageView.transform = CGAffineTransformMakeScale(0.9, 0.9);
    [VknImageCache loadImage:article.imageUrl defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        if(![article.imageUrl isEqualToString:key]) return;
        [VknThreadUtil mainBlock:^{
            [indicator stopAnimating];
            imageView.image = image;
            if(useCache){
                imageView.alpha = 1.0;
                imageView.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }else{
                [UIView animateWithDuration:0.2 animations:^{
                    imageView.alpha = 1.0;
                    imageView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                }];
            }
        }];
    }];
}

-(Article*)article{
    return article;
}

-(void)light{
    lightBg.alpha = 1.0f;
    [UIView animateWithDuration:0.4f animations:^{
        lightBg.alpha = 0.0f;
    } completion:^(BOOL finished) {
        
    }];
}

@end
