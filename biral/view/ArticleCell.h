//
//  ArticleCell.h
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ArticleCellDelegate <NSObject>

-(void) onSelectArticle:(Article*)article;

@end

@interface ArticleCell : UITableViewCell

-(void) setDelegate:(NSObject<ArticleCellDelegate>*)delegate_;
-(void) reload:(Article*)article;
-(void) light;
-(Article*)article;

@end
