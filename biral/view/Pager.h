//
//  Pager.h
//  mediabox
//
//  Created by kawase yu on 2014/08/02.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Pager : UIView

-(void) reload:(SiteList*)siteList;
-(void) page:(int)page;

@end
