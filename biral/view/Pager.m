//
//  Pager.m
//  mediabox
//
//  Created by kawase yu on 2014/08/02.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "Pager.h"

@interface Pager (){
    NSArray* list;
}

@end

@implementation Pager

-(id) init{
    self = [super init];
    if(self){
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    
}

#pragma mark -------- public -----------

-(void) reload:(SiteList*)siteList{
    for(UIImageView* iv in list){
        [iv removeFromSuperview];
    }
    
    CGRect frame = CGRectMake(0, 0, 6, 6);
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    for(int i=0,max=[siteList count];i<max;i++){
        Site* site = [siteList get:i];
        NSString* onImageName = [NSString stringWithFormat:@"page%d", /*i*/ 0];
        UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pageOff"] highlightedImage:[UIImage imageNamed:onImageName]];
        imageView.frame = frame;
        [tmp addObject:imageView];
        [self addSubview:imageView];
        frame.origin.y += 13.5f;
    }
    
    list = [NSArray arrayWithArray:tmp];
    self.frame = CGRectMake(10, 27.5f, 6, frame.origin.y);
    
    [self page:0];
}

-(void) page:(int)page{
    for(UIImageView* iv in list){
        iv.highlighted = NO;
        iv.alpha = 0.5f;
    }
    
    UIImageView* iv = [list objectAtIndex:page];
    iv.highlighted = YES;
    iv.alpha = 1.0f;
}

@end
