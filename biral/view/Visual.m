//
//  Visual.m
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "Visual.h"

@interface Visual (){
    __weak NSObject<VisualDelegate>* delegate;
    UIView* view;
    UIView* imageWrap;
    UILabel* siteLabel;
    UILabel* titleLabel;
    UILabel* categoryLabel;
    UIImageView* imageView;
    float scale;
    ArticleList* articleList;
    UIImageView* kidoku;
    int currentIndex;
    int fadeStep;
}

@end

@implementation Visual

static NSArray* instanceList;
static NSTimer* timer;
+(Visual*)createWithDelegate:(NSObject<VisualDelegate>*)delegate_{
    Visual* visual = [[Visual alloc] initWithDelegate:delegate_];
    if(instanceList == nil){
        instanceList = [[NSArray alloc] init];
    }
    NSMutableArray* tmp = [instanceList mutableCopy];
    [tmp addObject:visual];
    instanceList = [NSArray arrayWithArray:tmp];
    
    return visual;
}

+(void) startTimer{
    [timer invalidate];
    timer = nil;
    timer = [NSTimer timerWithTimeInterval:1.0f/30.0f target:self selector:@selector(onStaticTime) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

+(void) onStaticTime{
    for(Visual* visual in instanceList){
        [visual onTime];
    }
}

-(id) initWithDelegate:(NSObject<VisualDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    
    // image
    imageWrap = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    imageWrap.clipsToBounds = YES;
    [view addSubview:imageWrap];
    
    imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(0, 0, 320, 320);
    imageView.contentMode =  UIViewContentModeScaleAspectFill;
    [imageWrap addSubview:imageView];
    
    UIImageView* shadow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"visualShadow"]];
    shadow.frame = CGRectMake(0, [VknDeviceUtil windowHeight]-345.5f, 320, 97.5f);
    [view addSubview:shadow];
    
    // はんこ
    kidoku = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"visualKidoku"]];
    kidoku.frame = CGRectMake(193, [VknDeviceUtil windowHeight]-388.0f, 66, 65);
    [view addSubview:kidoku];
    
    // labels
    siteLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, [VknDeviceUtil windowHeight]-378.0f, 290, 33)];
    siteLabel.font = [UIFont boldSystemFontOfSize:30.0f];
    [view addSubview:siteLabel];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, [VknDeviceUtil windowHeight]-339, 290, 29)];
    titleLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    titleLabel.numberOfLines = 2;
    [view addSubview:titleLabel];
    
    categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, [VknDeviceUtil windowHeight]-304, 290, 10)];
    categoryLabel.font = [UIFont systemFontOfSize:10.0f];
    [view addSubview:categoryLabel];
    
    siteLabel.textColor = titleLabel.textColor = categoryLabel.textColor = [UIColor whiteColor];
    [[self getView] addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)]];
    
    titleLabel.layer.shadowColor = siteLabel.layer.shadowColor = categoryLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    titleLabel.layer.shadowOffset = siteLabel.layer.shadowOffset= categoryLabel.layer.shadowOffset = CGSizeMake(1, 1);
    titleLabel.layer.shadowOpacity = siteLabel.layer.shadowOpacity = categoryLabel.layer.shadowOpacity = 0.4f;
    titleLabel.layer.shadowRadius = siteLabel.layer.shadowRadius = categoryLabel.layer.shadowRadius = 1;
    
}

-(void) tap{
    if([articleList count] == 0) return;
    
    UIView* whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    whiteView.backgroundColor = [UIColor whiteColor];
    whiteView.alpha = 0.5f;
    [view addSubview:whiteView];
    [UIView animateWithDuration:0.4f animations:^{
        whiteView.alpha = 0;
    } completion:^(BOOL finished) {
        [whiteView removeFromSuperview];
    }];
    
    Article* article = [articleList get:currentIndex];
    [VknThreadUtil mainBlock:^{
        [delegate onSelectVisual:article];
        kidoku.hidden = NO;
    }];
}

-(UIView*) getView{
    return view;
}

-(void) reload:(Site*)site{
    currentIndex = 0;
    articleList = [site.articleList visualList];
    if([articleList count] == 0) return;
    Article* article = [articleList get:currentIndex];
    
    titleLabel.text = article.title;
    siteLabel.text = site.name;
    categoryLabel.text = [article category];
    
    imageView.image = nil;
    [VknImageCache loadImage:article.imageUrl defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        if(![key isEqualToString:article.imageUrl]) return;
        [VknThreadUtil mainBlock:^{
            imageView.image = image;
        }];
    }];
    
    [self resetScale];
}

-(void) resetScale{
    scale = 1.0f;
    imageView.alpha = 0.0f;
    fadeStep = 0;
}

-(void) onTime{
    if(fadeStep < 30*7){
        [self step1];
    }else if(fadeStep < 30*9){
        [self step2];
    }else{
        [self next];
        return;
    }
    fadeStep++;
}

-(void) step1{
    imageView.alpha += 1.0f / (30.0f*5.0f);
    scale += 0.0010f;
    imageView.transform = CGAffineTransformMakeScale(scale, scale);
}

-(void) step2{
    imageView.alpha -= 1.0f / (30.0f*2.0f);
}

-(void) next{
    currentIndex++;
    if([articleList count] <= currentIndex) currentIndex = 0;
    if([articleList count] == 0) return;
    Article* article = [articleList get:currentIndex];
    titleLabel.text = article.title;
    categoryLabel.text = [article category];
    kidoku.hidden = !article.readed;
    
    imageView.image = nil;
    [VknImageCache loadImage:article.imageUrl defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        if(![key isEqualToString:article.imageUrl]) return;
        [VknThreadUtil mainBlock:^{
            imageView.image = image;
        }];
    }];
    
    [self resetScale];
}

@end
