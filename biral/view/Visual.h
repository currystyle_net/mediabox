//
//  Visual.h
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VisualDelegate <NSObject>

-(void) onSelectVisual:(Article*)article;

@end

@interface Visual : NSObject

+(Visual*)createWithDelegate:(NSObject<VisualDelegate>*)delegate_;
-(UIView*) getView;
-(void) reload:(Site*)site;
+(void) startTimer;

@end
