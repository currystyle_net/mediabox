//
//  Ad.m
//  biral
//
//  Created by kawase yu on 2014/07/26.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import "Ad.h"
#import "AdArticle.h"
#import "ChkController.h"
#import "ChkControllerDelegate.h"

@interface Ad ()<ChkControllerDelegate>{
    
}

@end

@implementation Ad

#pragma mark -- ChkControllerDelegate --

- (void) chkControllerDataListWithSuccess:(NSDictionary *)data{
    
    if( [chkController hasNextData] ) {
        NSLog(@"****** self.chkController retry request *****");
        [chkController requestDataList];
        return;
    }
    
    [articleList clear];
    NSArray* dataList = [chkController dataList];
    for(ChkRecordData* data in dataList){
        AdArticle* adArticle = [[AdArticle alloc] initWithChkdata:data];
        [articleList add:adArticle];
    }
}

- (void) chkControllerDataListWithError:(NSError*)error{
    NSLog(@"chkControllerDataListWithError:%@", error.debugDescription);
}

- (void) chkControllerDataListWithNotFound:(NSDictionary *)data{
    NSLog(@"chkControllerDataListWithNotFound");
    NSLog(@"%@", data);
}

static Ad* ad;
static ChkController* chkController;
static ArticleList* articleList;
static AdArticle* defaultAd;
+(void)requestData{
    ad = [[Ad alloc] init];
    chkController = [[ChkController alloc] initWithDelegate:ad];
    [chkController requestDataList];
    articleList = [[ArticleList alloc] init];
    
    // 読み込むまでの一時しのぎ
    [articleList add:[AdArticle createKaimono]];
    [articleList add:[AdArticle createSeiji]];
    [articleList add:[AdArticle createYakuzai]];
}

+(Article*)get{
    int r = [VknUtils randIntRange:NSMakeRange(0, [articleList count]-1)];
    AdArticle* article = (AdArticle*)[articleList get:r];
    return article;
}

@end
