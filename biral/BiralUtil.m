//
//  BiralUtil.m
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import <Social/Social.h>
#import "BiralUtil.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import "RootViewController.h"

#define GA_TRACKING_CODE @"UA-53116668-1"

@interface BiralUtil ()

+(void) noReview;

@end

@interface AlertDelegate : NSObject<UIAlertViewDelegate>

@end

@implementation AlertDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        // レビューする
        [VknUtils openBrowser:@"https://itunes.apple.com/jp/app/id903695563?l=ja&ls=1&mt=8"];
        [BiralUtil noReview];
    }else if(buttonIndex == 1){
        // あとで
        return;
    }else if(buttonIndex == 2){
        // レビューしない
        [BiralUtil noReview];
    }
}

@end

@implementation BiralUtil


static RootViewController* rootViewController;
static UIScrollView* mainSv;
static NSObject<PageDelegate>* pageDelegate;
static NSMutableArray* readedList;

static BOOL isJaDevice;
static NSString* const readedListKey = @"readedListKey";
+(void) setup:(UIViewController*)rootViewController_
       mainSv:(UIScrollView *)mainSv_
 pageDelegate:(NSObject<PageDelegate>*)pageDelegate_{
    rootViewController = rootViewController_;
    mainSv = mainSv_;
    pageDelegate = pageDelegate_;
    // analytics
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 10;
//    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelError];
    [[GAI sharedInstance] trackerWithTrackingId:GA_TRACKING_CODE];
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    NSArray* list = [ud arrayForKey:readedListKey];
    readedList = [[NSMutableArray alloc] init];
    if(list != nil){
        readedList = [list mutableCopy];
    }
    
    CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [netinfo subscriberCellularProvider];
    NSString* countryCode = carrier.mobileCountryCode;
    isJaDevice = ([countryCode isEqualToString:@"440"] || [countryCode isEqualToString:@"441"]);
}


// global
+(UIScrollView*)mainSv{
    return mainSv;
}

+(UIViewController*)rootViewController{
    return rootViewController;
}

+(NSObject<PageDelegate>*)pageDelegate{
    return pageDelegate;
}

+(void) trackScreen:(NSString*)screenName{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:screenName];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

+(void) shareTw:(Article*)article{
    [self showComposeViewController:SLServiceTypeTwitter article:article];
}

+(void) shareFb:(Article*)article{
    [self showComposeViewController:SLServiceTypeFacebook article:article];
}

+(void) shareLine:(Article*)article{
    NSString* lineText = [NSString stringWithFormat:@"%@ \n%@", article.title, article.linkUrl];
    NSString *contentType = @"text";
    NSString *contentKey = (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                        NULL,
                                                                                        (CFStringRef)lineText,
                                                                                        NULL,
                                                                                        (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                        kCFStringEncodingUTF8 );
    NSString *urlString2 = [NSString
                            stringWithFormat:@"line://msg/%@/%@",
                            contentType, contentKey];
    NSURL *url = [NSURL URLWithString:urlString2];
    [[UIApplication sharedApplication] openURL:url];
}

typedef void (^HintUtilCompolseView_t)(SLComposeViewController* composeViewController);
+(void) showComposeViewController:(NSString* const)serviceType
                          article:(Article*)article{
    
    SLComposeViewController* slComposeViewController = [SLComposeViewController composeViewControllerForServiceType:serviceType];
    
    NSString* shareText;
    
    // twitterのとき
    NSString* appUrl = @"http://goo.gl/NBfjsr";
    if(serviceType == SLServiceTypeTwitter){
        shareText = [NSString stringWithFormat:@"%@, %@", article.title, @"#mediaboxApp"];
    }if(serviceType == SLServiceTypeFacebook){
        shareText = [NSString stringWithFormat:@"%@ \%@\n%@", article.title, @"Mediabox", appUrl];
    }
    
    [slComposeViewController setInitialText:shareText];
    [slComposeViewController addURL:[self shareUrl:article]];
    [slComposeViewController addImage:[UIImage imageNamed:@"icon"]];
    [rootViewController presentViewController:slComposeViewController animated:YES completion:^{
        
    }];
    
    return;
    
    [rootViewController showLoading];
    [VknImageCache loadImage:article.imageUrl defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        [slComposeViewController addImage:image];
        [VknThreadUtil mainBlock:^{
            [rootViewController hideLoading];
            [rootViewController presentViewController:slComposeViewController animated:YES completion:^{
                
            }];
        }];
    }];
}

+(NSURL*)shareUrl:(Article*)article{
    return [NSURL URLWithString:article.linkUrl];
    //    api.hint.vikana.net
    //    return [NSURL URLWithString:[NSString stringWithFormat:@"http://api.hint.vikana.net/page/?id=%d", article.id_]];
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://web.hint.vc/page/?id=%d", article.id_]];
    //    return [NSURL URLWithString:@"http://goo.gl/bHNNMB"];
}

+(void) pain:(UIView*)view{
    view.transform = CGAffineTransformMakeScale(0.97f, 0.97f);
    view.alpha = 0.6f;
    [UIView animateWithDuration:0.2f animations:^{
        view.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
        view.alpha = 1.0f;
    }];
}


static NSString* twUsernameKey = @"twUsernameKey";
+(void)currntTwAccount:(BiralUtilACAccountCallback_t)callback{
    NSString* currentTwUsername = [[NSUserDefaults standardUserDefaults] stringForKey:twUsernameKey];
    
    ACAccountStore *store = [[ACAccountStore alloc] init];
    ACAccountType *type = [store accountTypeWithAccountTypeIdentifier:
                           ACAccountTypeIdentifierTwitter];
    
    [store requestAccessToAccountsWithType:type
                                   options:nil
                                completion:^(BOOL granted, NSError *error) {
                                    
                                    if (!granted) {
                                        NSLog(@"not granted");
                                        [VknThreadUtil mainBlock:^{
                                            callback(nil);
                                        }];
                                        return;
                                    }
                                    
                                    NSArray *twitterAccounts = [store accountsWithAccountType:type];
                                    
                                    if (!(twitterAccounts > 0)) {
                                        NSLog(@"no twitter accounts");
                                        [VknThreadUtil mainBlock:^{
                                            callback(nil);
                                        }];
                                        return;
                                    }
                                    
                                    // あるかなー
                                    for(ACAccount* account in twitterAccounts){
                                        if([account.username isEqualToString:currentTwUsername]){
                                            [VknThreadUtil mainBlock:^{
                                                callback(account);
                                            }];
                                            return;
                                        }
                                    }
                                    
                                    // ないね
                                    [VknThreadUtil mainBlock:^{
                                        callback(nil);
                                    }];
                                }];
}

+(void) setCurrentTwusername:twUsername{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:twUsername forKey:twUsernameKey];
    [ud synchronize];
}

static NSString* fbUsernameKey = @"fbUsernameKey";
+(void)currentFbAccount:(BiralUtilACAccountCallback_t)callback{
    NSString* currentFbUsername = [[NSUserDefaults standardUserDefaults] stringForKey:fbUsernameKey];
    
    
}

+(void) setCurrentFbusername:(NSString*)fbUsername{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:fbUsername forKey:fbUsernameKey];
    [ud synchronize];
}

+(BOOL)isReaded:(int)articleId{
    NSNumber* nn = [NSNumber numberWithInteger:articleId];
    for(NSNumber* n in readedList){
        if([n intValue] == [nn intValue]) return YES;
    }
    
    return NO;
}

+(void) read:(int)articleId{
    NSNumber* nn = [NSNumber numberWithInteger:articleId];
    if([readedList indexOfObject:nn] != NSNotFound) return;
    
    [readedList addObject:nn];
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSArray arrayWithArray:readedList] forKey:readedListKey];
    [ud synchronize];
}

static NSString* showedCountKey = @"showedCountKey";
static AlertDelegate* alertDelegate ;
+(void) tryShowRecommend{
    if([self isNoReview]) return;
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    int current = [ud integerForKey:showedCountKey];
    current++;
    [ud setInteger:current forKey:showedCountKey];
    [ud synchronize];
    
    NSLog(@"count:%d", current);
    
    if(!(current == 5 || current == 15 || current == 40 || current == 100)) return;
    
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.title = @"お願いします";
    alertView.message = @"いつもご利用ありがとうございます。\nもし今お時間ありましたら、レビューにてご意見ご感想を書いていただけないでしょうか。\n開発の励みになりますm(^_^)m。";
    [alertView addButtonWithTitle:@"レビューする"];
    [alertView addButtonWithTitle:@"あとで"];
    [alertView addButtonWithTitle:@"レビューしない"];
    
    alertDelegate = [[AlertDelegate alloc] init];
    alertView.delegate = alertDelegate;
    
    [alertView show];
}

static NSString* noReviewKey = @"noReviewKey";
+(BOOL) isNoReview{
    return [[NSUserDefaults standardUserDefaults] boolForKey:noReviewKey];
}

+(void) noReview{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:noReviewKey];
    [ud synchronize];
}

+(BOOL) isJaDevice{
    return isJaDevice;
}

@end
