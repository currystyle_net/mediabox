//
//  BiralUtil.h
//  biral
//
//  Created by kawase yu on 2014/07/21.
//  Copyright (c) 2014年 currystyle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PageDelegate.h"
#import <Accounts/Accounts.h>

@class Article;

typedef void (^BiralUtilACAccountCallback_t)(ACAccount* account);

@interface BiralUtil : NSObject

+(BOOL) isJaDevice;

+(void) setup:(UIViewController*)rootViewController_
       mainSv:(UIScrollView*)mainSv_
 pageDelegate:(NSObject<PageDelegate>*)pageDelegate_;

+(UIViewController*)rootViewController;
+(UIScrollView*)mainSv;
+(NSObject<PageDelegate>*)pageDelegate;
+(void) trackScreen:(NSString*)screenName;

+(void) shareTw:(Article*)article;
+(void) shareFb:(Article*)article;
+(void) shareLine:(Article*)article;

+(void) pain:(UIView*)view;

+(void)currntTwAccount:(BiralUtilACAccountCallback_t)callback;
+(void) setCurrentTwusername:(NSString*)twUsername;

+(void)currentFbAccount:(BiralUtilACAccountCallback_t)callback;
+(void) setCurrentFbusername:(NSString*)fbUsername;

+(BOOL)isReaded:(int)articleId;
+(void) read:(int)articleId;

+(void) tryShowRecommend;

@end
